
var onSubmit = function (e) {
    e.preventDefault();

    var url = $('#imgUrl').val();
    var files = $('#imgFile')[0].files;
    var alt = $('#altCaption').val();
    var width = $('#width').val();
    var height = $('#height').val();

    var imgString = '<img alt="' + alt + '"' +
        (width ? ' width="' + width + '"' : '') +
        (height ? ' height="' + height + '"' : '') +
        ' />';
    var imgElm = $(imgString);

    if(files && files.length){
        var fr = new FileReader();
        fr.onload = function () {
            imgElm.attr('src', fr.result);
            $('.img-placeholder').html('').append(imgElm);
        };
        fr.readAsDataURL(files[0]);
    }else{
        imgElm.attr('src', url);
        $('.img-placeholder').html('').append(imgElm);
    }
};

var init = function () {
    $('form').on('submit', onSubmit);
};